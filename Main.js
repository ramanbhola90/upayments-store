import React, { useContext } from "react";
import { NavigationContainer } from "@react-navigation/native";

// Routes
import HomeStackScreen from "./src/routes/HomeStackScreen";

// Context
import { Ucontext } from "./src/context/Ucontext";

// Components
import Snack from "./src/components/common/Snack";

const Main = (props) => {
  // Prop Destructuring
  const {} = props;

  // Context Varibales
  const { snackState } = useContext(Ucontext);

  return (
    <NavigationContainer>
      <HomeStackScreen />
      {snackState.visible ? <Snack /> : null}
    </NavigationContainer>
  );
};

export default Main;
