import React, { useState, useEffect, useContext } from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

// Context
import { Ucontext } from "../context/Ucontext";

// Components
import InputField from "../components/input/InputField";
import CategoryCard from "../components/cards/CategoryCard";
import Button from "../components/buttons/Button";

const AddProduct = (props) => {
  // Prop Destructuring
  const { navigation } = props;

  // Context Variables
  const {
    categories,
    snackDispatch,
    selectedCategory,
    addProduct,
    getProducts,
    setSelectedCategory,
  } = useContext(Ucontext);

  // State Variables
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const [loading, setLoading] = useState(false);

  // Handles New Product Addition
  const submitHandler = async () => {
    setLoading(true);
    try {
      if (!title || !price || !description || !image || !selectedCategory) {
        setLoading(false);
        return snackDispatch({
          type: "SHOW_SNACK",
          payload: "Please enter all details !",
        });
      }

      const newProduct = await addProduct({
        name: title,
        price,
        category: selectedCategory.name,
        description,
        avatar: image,
        developerEmail: "testdeveloper@yopmail.com",
      });

      setSelectedCategory({});
      getProducts();

      navigation.goBack();
    } catch (err) {
      console.log("Some issue while Submitting form - ", err);
    }
    setLoading(false);
  };

  return (
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        {/* Sized Box */}
        <View style={{ height: hp(2) }} />

        <InputField
          title="Product title"
          placeHolder="Enter product title"
          value={title}
          setValue={setTitle}
        />

        {/* Sized Box */}
        <View style={{ height: hp(2) }} />

        <InputField
          title="Price"
          placeHolder="Enter product price"
          value={price}
          setValue={setPrice}
          keyboardType="numeric"
        />

        {/* Sized Box */}
        <View style={{ height: hp(2) }} />

        <InputField
          title="Description"
          placeHolder="Enter product description"
          value={description}
          setValue={setDescription}
        />

        {/* Sized Box */}
        <View style={{ height: hp(2) }} />

        <InputField
          title="Image Link"
          placeHolder="Enter product image link"
          value={image}
          setValue={setImage}
        />

        {/* Sized Box */}
        <View style={{ height: hp(2) }} />

        <View>
          <Text style={styles.textValue}>
            Selected Category: {selectedCategory?.name}
          </Text>
          <FlatList
            data={categories}
            renderItem={({ item }) => <CategoryCard item={item} />}
            keyExtractor={(item) => item.id}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>

      {/* Sized Box */}
      <View style={{ height: hp(6) }} />

      {/* Submit Button Section */}
      <View style={styles.addButtonContainer}>
        <Button title="Add Product" onPress={submitHandler} />
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: wp(6),
  },
  addButtonContainer: {
    width: wp(40),
    alignSelf: "center",
  },
  textValue: {
    fontSize: RFValue(12),
    textTransform: "capitalize",
  },
});

export default AddProduct;
