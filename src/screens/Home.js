import React, { useState, useEffect, useContext } from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

// Context
import { Ucontext } from "../context/Ucontext";

// Components
import ProductCard from "../components/cards/ProductCard";
import CategoryCard from "../components/cards/CategoryCard";
import FAB from "../components/buttons/FAB";

const Home = (props) => {
  // Prop Destructuring
  const { navigation } = props;

  // Context Variables
  const { products, categories, getProducts, getCategories } =
    useContext(Ucontext);

  return (
    <>
      {/* Category Section */}
      <View>
        <FlatList
          data={categories}
          renderItem={({ item }) => <CategoryCard item={item} />}
          keyExtractor={(item) => item.id}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
      {/* Products Section */}
      <View style={styles.container}>
        <FlatList
          data={products}
          renderItem={({ item }) => (
            <ProductCard item={item} navigation={navigation} />
          )}
          keyExtractor={(item) => item.id}
          columnWrapperStyle={styles.colContainer}
          horizontal={false}
          numColumns={2}
        />
      </View>

      {/* Floating Action Button */}
      <FAB navigation={navigation} />

      {/* Sized Box */}
      <View style={{ height: hp(4) }} />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  colContainer: {
    width: wp(100),
    paddingHorizontal: wp(6),
    justifyContent: "space-between",
  },
});

export default Home;
