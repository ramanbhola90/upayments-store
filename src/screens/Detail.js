import React from "react";
import { StyleSheet, Text, View, Image, ScrollView } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

// Constants
import { colors } from "../assets/colors";

const Detail = (props) => {
  // Prop Destructuring
  const { navigation } = props;
  const { name, avatar, description, price } = props.route.params.item;

  return (
    <View style={styles.container}>
      {/* Image Section */}
      <Image
        style={styles.productImage}
        source={{
          uri: avatar,
        }}
        resizeMode="cover"
      />

      {/* Detail Section */}
      <View style={styles.detailContainer}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={styles.detailTitle}>{name}</Text>
          <Text style={styles.detailTitle}>${price}</Text>
        </View>

        {/* Sized Box */}
        <View style={{ height: hp(2) }} />

        <ScrollView>
          <Text style={styles.detailDescription}>{description}</Text>

          {/* Sized Box */}
          <View style={{ height: hp(6) }} />
        </ScrollView>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  productImage: {
    width: wp(100),
    height: hp(40),
  },
  detailContainer: {
    top: -hp(6),
    width: wp(100),
    height: hp(60),
    backgroundColor: colors.primary,
    borderTopLeftRadius: hp(4),
    borderTopRightRadius: hp(4),
    paddingHorizontal: wp(4),
    paddingVertical: hp(2),
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    elevation: 5,
  },
  detailTitle: {
    fontSize: RFValue(17),
    color: "#ffffff",
    fontWeight: "bold",
  },
  detailDescription: {
    fontSize: RFValue(13),
    color: "#ffffff",
  },
});

export default Detail;
