import React, { useState, useEffect, useReducer } from "react";
import axios from "axios";

// Config
import { config } from "../config";

// Reducers
import { snackReducer, initialSnackState } from "./reducers/snackReducer";

export const Ucontext = React.createContext();

export const Uprovider = (props) => {
  // Reducer Variables
  const [snackState, snackDispatch] = useReducer(
    snackReducer,
    initialSnackState
  );

  // State Variables
  const [originalProducts, setOriginalProducts] = useState([]);
  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState({});

  // Mounting
  useEffect(() => {
    getCategories();
    getProducts();
  }, []);

  // Updating Phase
  useEffect(() => {
    if (selectedCategory.id) {
      const filteredProducts = originalProducts.filter(
        (product) => product.category === selectedCategory.name
      );
      setProducts(filteredProducts);
    } else {
      setProducts(originalProducts);
    }
    console.log("No selected category", selectedCategory);
  }, [selectedCategory]);

  // Common Methods
  const getProducts = async () => {
    try {
      const axiosRes = await axios({
        method: "get",
        url: `${config.BASE_URL}/products`,
      });

      setOriginalProducts(axiosRes.data);

      setProducts(axiosRes.data);

      return axiosRes.data;
    } catch (err) {
      console.log("Some issue while getting products - ", err);
    }
  };

  const getCategories = async () => {
    try {
      const axiosRes = await axios({
        method: "get",
        url: `${config.BASE_URL}/categories`,
      });

      setCategories(axiosRes.data);

      return axiosRes.data;
    } catch (err) {
      console.log("Some issue while getting categories - ", err);
    }
  };

  const deleteProduct = async ({ id }) => {
    try {
      const axiosRes = await axios({
        method: "delete",
        url: `${config.BASE_URL}/products/${id}`,
      });

      getProducts();

      return axiosRes.data;
    } catch (err) {
      console.log("Some issue while deleting product - ", err);
    }
  };

  const addProduct = async ({
    name,
    price,
    category,
    description,
    avatar,
    developerEmail,
  }) => {
    try {
      const axiosRes = await axios({
        method: "post",
        url: `${config.BASE_URL}/products`,
        data: { name, price, category, description, avatar, developerEmail },
      });

      getProducts();

      return axiosRes.data;
    } catch (err) {
      console.log("Some issue while adding product - ", err);
    }
  };

  return (
    <Ucontext.Provider
      value={{
        // Common State
        products,
        setProducts,
        categories,
        setCategories,
        selectedCategory,
        setSelectedCategory,
        // Reducers
        snackState,
        snackDispatch,
        // Common Methods
        getProducts,
        getCategories,
        deleteProduct,
        addProduct,
      }}
    >
      {props.children}
    </Ucontext.Provider>
  );
};
