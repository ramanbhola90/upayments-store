import React, { useEffect, useContext } from "react";
import { StyleSheet, Text, View } from "react-native";
import SnackBar from "react-native-snackbar-component";

// Context
import { Ucontext } from "../../context/Ucontext";
import { colors } from "../../assets/colors";

const Snack = () => {
  // Context Variables
  const { snackState, snackDispatch } = useContext(Ucontext);

  // Mounting
  useEffect(() => {
    const snackTimer = setTimeout(() => {
      snackDispatch({ type: "HIDE_SNACK" });
    }, 5000);
    return () => {
      clearTimeout(snackTimer);
    };
  }, []);

  return (
    <SnackBar
      visible={snackState.visible}
      textMessage={snackState.message}
      actionHandler={() => snackDispatch({ type: "HIDE_SNACK" })}
      actionText="DISMISS"
      accentColor={colors.primary}
    />
  );
};

export default Snack;
