import React, { useState, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { MaterialIcons } from "@expo/vector-icons";

// Context
import { Ucontext } from "../../context/Ucontext";

// Constants
import { colors } from "../../assets/colors";

const ProductCard = (props) => {
  // Prop Destructuring
  const { navigation } = props;
  const { avatar, name, description, price, id } = props.item;

  // Context Variables
  const { deleteProduct, snackDispatch } = useContext(Ucontext);

  // State Variables
  const [loading, setLoading] = useState(false);

  const deleteProductHandler = async (id) => {
    setLoading(true);
    try {
      const deletedProduct = await deleteProduct({ id });

      console.log("The delete Response - ", deletedProduct);
    } catch (err) {
      console.log("Some issue while deleting product - ", err);
    }
    setLoading(false);
  };

  // Alert handler
  const deleteAlert = () =>
    Alert.alert("Delete product", `Are you sure you want to delete ${name} ?`, [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      { text: "OK", onPress: () => deleteProductHandler(id) },
    ]);

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={() =>
        navigation.push("DetailScreen", { item: props.item, headerTitle: name })
      }
    >
      {/* Product Image Section */}
      <Image
        style={styles.productImage}
        source={{
          uri: avatar,
        }}
        resizeMode="cover"
      />

      {/* Product Details Section */}
      <View style={styles.detailContainer}>
        <Text style={styles.productName}>{name}</Text>

        {/* Sized Box */}
        <View style={{ height: hp(1) }} />

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Text style={styles.productPrice}>${price}</Text>
          {loading ? (
            <ActivityIndicator size="small" color="#fff" />
          ) : (
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.deleteButton}
              onPress={deleteAlert}
            >
              <MaterialIcons name="delete" size={hp(3)} color="red" />
            </TouchableOpacity>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    // height: hp(24),
    width: wp(42),
    marginBottom: hp(1.5),
    borderRadius: hp(1),
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    elevation: 5,
  },
  detailContainer: {
    backgroundColor: colors.primary,
    flex: 1,
    borderBottomLeftRadius: hp(1),
    borderBottomRightRadius: hp(1),
    paddingHorizontal: wp(1),
    paddingVertical: hp(1),
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    elevation: 5,
  },
  productImage: {
    width: "100%",
    height: hp(18),
    alignSelf: "center",
  },
  productName: {
    fontSize: RFValue(12),
    fontWeight: "bold",
    color: "#ffffff",
  },
  productPrice: {
    fontSize: RFValue(11),
    fontWeight: "bold",
    color: "#ffffff",
  },
  deleteButton: {
    // backgroundColor: "yellow",
  },
});

export default ProductCard;
