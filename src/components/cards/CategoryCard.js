import React, { useContext } from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

// Context
import { Ucontext } from "../../context/Ucontext";

// Constants
import { colors } from "../../assets/colors";

const CategoryCard = (props) => {
  // Prop Destructuring
  const { id, name } = props.item;

  // Context Variables
  const { selectedCategory, setSelectedCategory } = useContext(Ucontext);

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={{
        ...styles.container,
        backgroundColor: selectedCategory.id === id ? "#fff" : colors.primary,
      }}
      onPress={() =>
        selectedCategory?.id === id
          ? setSelectedCategory({})
          : setSelectedCategory(props.item)
      }
    >
      <Text
        style={{
          ...styles.cardText,
          color: selectedCategory.id === id ? colors.primary : "#fff",
        }}
      >
        {name}
      </Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    paddingHorizontal: wp(4),
    paddingVertical: hp(1),
    marginHorizontal: wp(2),
    marginVertical: hp(2),
    borderRadius: hp(1),
    borderWidth: 1,
    borderColor: colors.primary,
  },
  cardText: {
    fontSize: RFValue(12),
    fontWeight: "bold",
    textTransform: "capitalize",
    color: "#ffffff",
  },
});

export default CategoryCard;
