import React from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

// Constants
import { colors } from "../../assets/colors";

const InputField = (props) => {
  // Prop Destructuring
  const {
    title = "",
    value = "",
    setValue = () => {},
    placeHolder = "",
    keyboardType = "default",
  } = props;

  return (
    <View>
      <Text style={styles.inputTitle}>{title}</Text>

      {/* Sized Box */}
      <View style={{ height: hp(0.7) }} />

      <TextInput
        style={styles.inputField}
        placeholder={placeHolder}
        value={value}
        onChangeText={(text) => setValue(text)}
        keyboardType={keyboardType}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  inputTitle: {
    fontSize: RFValue(11),
    color: colors.primary,
  },
  inputField: {
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: hp(1),
    paddingHorizontal: wp(2),
    paddingVertical: hp(1.5),
    fontSize: RFValue(13),
  },
});

export default InputField;
