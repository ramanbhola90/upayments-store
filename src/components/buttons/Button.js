import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

// Constants
import { colors } from "../../assets/colors";

const Button = (props) => {
  // Prop Destructuring
  const { title = "", onPress = () => {} } = props;
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={() => onPress()}
      style={styles.container}
    >
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    paddingVertical: hp(1.5),
    paddingHorizontal: wp(4),
    borderRadius: hp(1),
  },
  text: {
    fontSize: RFValue(14),
    color: "#ffffff",
    fontWeight: "bold",
    textAlign: "center",
  },
});

export default Button;
