import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

// Constants
import { colors } from "../../assets/colors";

const FAB = (props) => {
  // Prop Destructuring
  const { navigation } = props;

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={() => navigation.push("AddProduct")}
    >
      <AntDesign name="pluscircleo" size={hp(6)} color="black" />
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    position: "absolute",
    bottom: hp(4),
    right: wp(4),
    borderRadius: hp(100),
  },
});

export default FAB;
