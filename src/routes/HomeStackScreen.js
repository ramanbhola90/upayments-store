import React from "react";
import { TouchableOpacity } from "react-native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Feather } from "@expo/vector-icons";

// Screens
import Home from "../screens/Home";
import Detail from "../screens/Detail";
import AddProduct from "../screens/AddProduct";

const HomeStack = createNativeStackNavigator();

const HomeStackScreen = ({ navigation, route }) => {
  return (
    <HomeStack.Navigator initialRouteName="HomeScreen">
      <HomeStack.Screen
        options={{
          title: "Upayments Store",
          headerRight: () => (
            <TouchableOpacity activeOpacity={0.7}>
              <Feather name="search" size={hp(2.5)} color="black" />
            </TouchableOpacity>
          ),
        }}
        name="HomeScreen"
        component={Home}
      />
      <HomeStack.Screen
        options={({ route }) => ({ title: route.params.headerTitle })}
        name="DetailScreen"
        component={Detail}
      />
      <HomeStack.Screen
        options={{
          title: "Add product",
        }}
        name="AddProduct"
        component={AddProduct}
      />
    </HomeStack.Navigator>
  );
};

export default HomeStackScreen;
