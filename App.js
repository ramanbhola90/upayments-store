import React from "react";

// Context
import { Uprovider } from "./src/context/Ucontext";

// Components
import Main from "./Main";

const App = (props) => {
  // Prop Destructuring
  const {} = props;

  return (
    <Uprovider>
      <Main />
    </Uprovider>
  );
};

export default App;
